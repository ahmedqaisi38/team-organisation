package com.khairo.teamorganization.utils

import android.content.Context
import android.content.res.Configuration
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.khairo.teamorganization.maneger.SharedPreferencesManager
import java.util.*

object AppLogger  {

    fun toast (context: Context, message:String ){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun log(tag:String,msg:String){
        Log.d(tag, msg)
    }

    fun snackBar(view: View, message: String) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
            .setAction("Action", null).show()
    }

    fun setLang(context: Context, lang: String, langCheck: Int) {
        val mSharedPreference: SharedPreferencesManager by lazy { SharedPreferencesManager() }

        val  loc= Locale(lang)
        Locale.setDefault(loc)

        val con= Configuration()
        con.locale= loc
        context.resources.updateConfiguration(con, context.resources.displayMetrics)

        mSharedPreference.setIntPreferencesLang("lang", langCheck)
        Objects.lang = langCheck
    }
}
