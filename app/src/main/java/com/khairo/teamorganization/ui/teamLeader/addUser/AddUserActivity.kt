package com.khairo.teamorganization.ui.teamLeader.addUser

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.khairo.teamorganization.R
import com.khairo.teamorganization.maneger.SharedPreferencesManager
import com.khairo.teamorganization.utils.AppLogger
import com.khairo.teamorganization.utils.SlideAnimation
import kotlinx.android.synthetic.main.activity_add_user.*

class AddUserActivity : AppCompatActivity(), AddUserContract {

    private val mSharedPreference: SharedPreferencesManager by lazy { SharedPreferencesManager() }

    private val presenter= AddUserPresenter(this, AddUserInteractor())

    var type= ""

    var types= ArrayList<String>()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)

        name.text= getString(R.string.welcome)+ " "+ mSharedPreference.getStringPreferences("username", "")

        types.addAll(resources.getStringArray(R.array.users_types))

        type_sppiner!!.onItemSelectedListener = TypeClass()
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, resources.getStringArray(R.array.users_types))
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        type_sppiner.adapter = arrayAdapter

        menu_close.setOnClickListener {
            if (supports.visibility== View.GONE) {
                menuClose(view, 160, 570, 0)

            } else {
                menuClose(view, 570, 160, 1)

            }
        }

        add.setOnClickListener {
            checkAddUser(
                mSharedPreference.getIntPreferences("id", 0),
                user_name.text.toString().trim(),
                user_id.text.toString().trim(),
                password.text.toString().trim(),
                type,
                box.text.toString().trim(),
                home.text.toString().trim(),
                joint.text.toString().trim()
            )
        }
    }

    inner class TypeClass : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            type= types[position]

            if (position== 0) {
                b_h_j.visibility= View.VISIBLE

            } else {
                b_h_j.visibility= View.GONE
            }
        }
    }

    private fun menuClose(view: View, from: Int, to: Int, type: Int) {
        if (type== 0) {
            menu_close.setImageResource(R.drawable.ic_close)
            supports.visibility= View.VISIBLE

        } else {
            menu_close.setImageResource(R.drawable.ic_menu)
            supports.visibility= View.GONE

        }

        val animation: Animation = SlideAnimation(view, from, to)
        animation.interpolator = AccelerateInterpolator()
        animation.duration = 300
        view.animation = animation
        view.startAnimation(animation)

    }

    private fun checkAddUser(id: Int, username: String, userID: String, password: String, userType: String,
                     box: String, home: String, joint: String) {
        presenter.checkAddUser(id, username, userID, password, userType, box, home, joint)
    }

    override fun showProgress() {
        progressBar.visibility= View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility= View.GONE
    }

    override fun setUserNameError() {
        user_name.error= getString(R.string.username_error)
    }

    override fun setIDError() {
        user_id.error= getString(R.string.user_id_error)
    }

    override fun setPasswordError() {
        password.error= getString(R.string.user_password_error)
    }

    override fun setBoxError() {
        box.error= getString(R.string.user_box_error)
    }

    override fun setHomeError() {
        home.error= getString(R.string.user_home_error)
    }

    override fun setJointError() {
        joint.error= getString(R.string.user_joint_error)
    }

    override fun toast(message: String) {
        AppLogger.toast(this, message)
    }

    companion object {
        fun start(context: Context) {
            val intent= Intent(context, AddUserActivity::class.java)
            context.startActivity(intent)
        }
    }
}
