package com.khairo.teamorganization.ui.teamLeader.accounts

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.khairo.teamorganization.R
import com.khairo.teamorganization.utils.AppLogger
import kotlinx.android.synthetic.main.user_item.view.*

class UsersAdapter(private var con: Context, private var list: ArrayList<UsersData>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {

        (p0 as ItemView).bind(list[p1].user_id, list[p1].Username, list[p1].Usertype)

       p0.itemView.view_info.setOnClickListener {
           AppLogger.toast(con, "I will Add this later ^_^")
       }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        val v= LayoutInflater.from(con).inflate(R.layout.user_item, p0, false)

        return ItemView(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ItemView(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(id: Int, userName:String, userType:String) {
            if (userType!= "Worker") {
                itemView.view_info.text= ""
            }

            itemView.user_id.text= id.toString()
            itemView.user_name.text= userName
            itemView.user_type.text= userType
        }
    }
}
