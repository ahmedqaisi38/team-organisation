package com.khairo.teamorganization.ui.admin

import android.content.Context

class AdminPresenter(private val adminContract: AdminContract,
                     private val adminInteractor: AdminInteractor) :
        AdminInteractor.OnAFinishedListener {

    fun checkAddTeamLeader(teamName: String, teamID: String, password: String) {
        adminContract.showProgress()
        adminInteractor.checkAddTeamLeader(teamName, teamID, password, this)
    }

    fun changePassword(id: Int, context: Context) {
        adminInteractor.changePassword(id, context, this)
    }

    override fun toast(message: String) {
        adminContract.apply {
            hideProgress()
            toast(message)
        }
    }

    override fun onNameError() {
        adminContract.apply {
            hideProgress()
            setNameError()
        }
    }

    override fun onIDError() {
        adminContract.apply {
            hideProgress()
            setIDError()
        }
    }

    override fun onPasswordError() {
        adminContract.apply {
            hideProgress()
            setPasswordError()
        }
    }

    override fun onSuccess(teamName: String, teamID: Int, password: String) {
        adminInteractor.addTeamLeader(teamName, teamID, password, this)
    }

    override fun clear() {
        adminContract.clear()
    }
}
