package com.khairo.teamorganization.ui.admin

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import androidx.appcompat.app.AppCompatActivity
import com.khairo.teamorganization.ui.MainActivity
import com.khairo.teamorganization.R
import com.khairo.teamorganization.maneger.SharedPreferencesManager
import com.khairo.teamorganization.utils.AppLogger
import com.khairo.teamorganization.utils.SlideAnimation
import kotlinx.android.synthetic.main.activity_admin.*

class AdminActivity : AppCompatActivity(), AdminContract {

    private val mSharedPreference: SharedPreferencesManager by lazy { SharedPreferencesManager() }

    private val presenter= AdminPresenter(this, AdminInteractor())

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)

        name.text= getString(R.string.welcome)+ " "+ mSharedPreference.getStringPreferences("username", "")
        menu_close.setOnClickListener {
            if (supports.visibility== View.GONE) {
                menuClose(view, 160, 650, 0)

            } else {
                menuClose(view, 650, 160, 1)

            }
        }

        admin_login.setOnClickListener {
            checkAddTeamLeader(
                team_name.text.toString().trim(),
                team_id.text.toString().trim(),
                password.text.toString().trim()
            )
        }

        change_password.setOnClickListener {
            changePassword(
                mSharedPreference.getIntPreferences("id", 0)
            )
        }

        logout.setOnClickListener {
            mSharedPreference.clearPreference()
            MainActivity.start(this)
            finish()
        }
    }

    private fun menuClose(view: View, from: Int, to: Int, type: Int) {
        if (type== 0) {
            menu_close.setImageResource(R.drawable.ic_close)
            supports.visibility= View.VISIBLE

        } else {
            menu_close.setImageResource(R.drawable.ic_menu)
            supports.visibility= View.GONE

        }

        val animation: Animation = SlideAnimation(view, from, to)
        animation.interpolator = AccelerateInterpolator()
        animation.duration = 300
        view.animation = animation
        view.startAnimation(animation)

    }

    private fun checkAddTeamLeader(teamName: String, teamID: String, password: String) {
        presenter.checkAddTeamLeader(teamName, teamID, password)
    }

    private fun changePassword(id: Int) {
        presenter.changePassword(id, this)
    }

    override fun showProgress() {
        progressBar.visibility= View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility= View.GONE
    }

    override fun setNameError() {
        team_name.error= getString(R.string.team_name_error)
    }

    override fun setIDError() {
        team_id.error= getString(R.string.team_id_error)
    }

    override fun setPasswordError() {
        password.error= getString(R.string.team_password_error)
    }

    override fun toast(message: String) {
        AppLogger.toast(this, message)
    }

    override fun clear() {
        team_name.setText("")
        team_id.setText("")
        password.setText("")
    }

    companion object {
        fun start(context: Context) {
            val intent= Intent(context, AdminActivity::class.java)
            context.startActivity(intent)
        }
    }
}
