package com.khairo.teamorganization.ui.auth

interface LoginContract {
    fun showProgress()
    fun hideProgress()
    fun setIDError()
    fun setPasswordError()
    fun toast(message:String)
    fun goToAdmin()
    fun goToLeader()
}