package com.khairo.teamorganization.ui.admin

import android.app.Dialog
import android.content.Context
import android.os.Handler
import android.view.Gravity
import com.khairo.teamorganization.R
import com.khairo.teamorganization.maneger.SharedPreferencesManager
import com.khairo.teamorganization.retrofit.Api
import com.khairo.teamorganization.utils.AppLogger
import kotlinx.android.synthetic.main.dialog_change_password.*
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback

class AdminInteractor {
    interface OnAFinishedListener {
        fun toast(message: String)
        fun onNameError()
        fun onIDError()
        fun onPasswordError()
        fun onSuccess(teamName: String, teamID: Int, password: String)
        fun clear()
    }

    fun checkAddTeamLeader(teamName: String, teamID: String, password: String, listener: OnAFinishedListener) {
        Handler().postDelayed({
            when {
                teamName.isEmpty() -> listener.onNameError()
                teamID.isEmpty() -> listener.onIDError()
                password.isEmpty() -> listener.onPasswordError()
                else -> listener.onSuccess(teamName, teamID.toInt(), password)
            }
        }, 1000)
    }

    fun addTeamLeader(teamName: String, teamID: Int, password: String, listener: OnAFinishedListener) {

        val call: Call<List<Any>> = Api.getData.addTeamLeader(teamName, teamID, password)
        call.enqueue(object : Callback<List<Any>> {

            override fun onResponse(call: Call<List<Any>>?, response: retrofit2.Response<List<Any>>?) {
                AppLogger.log("checking", response.toString())
                AppLogger.log("checking2", response!!.body().toString())

                val array= JSONArray(response.body())
                val json= array.getJSONObject(0)

                if (json.getInt("data")== 1) {
                    listener.clear()
                    listener.toast("TeamLeader Added Successfully")

                } else {
                    listener.toast("TeamLeader is exist")

                }
            }

            override fun onFailure(call: Call<List<Any>>?, t: Throwable?) {
                AppLogger.log("fails", "Failed to add TeamLeader: $t")
                listener.toast("Failed to add teamLeader!!")
            }
        })
    }

    fun changePassword(id: Int, context: Context, listener: OnAFinishedListener) {

        val mSharedPreference: SharedPreferencesManager by lazy { SharedPreferencesManager() }

        val dialog= Dialog(context)
        dialog.setContentView(R.layout.dialog_change_password)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setGravity(Gravity.CENTER)

        dialog.show()

        dialog.change_password.setOnClickListener {
            when {
                dialog.old.text.toString().trim().isEmpty() -> dialog.old.error= context.getString(R.string.old_password_error)
                dialog.neww.text.toString().trim().isEmpty() -> dialog.neww.error= context.getString(R.string.new_password_error)
                dialog.confirm.text.toString().trim().isEmpty() -> dialog.confirm.error= context.getString(R.string.confirm_password_error)
                dialog.neww.text.toString().trim()!= dialog.confirm.text.toString().trim()-> dialog.confirm.error= context.getString(R.string.confirm_not_much_error)

                else -> {
                    val call: Call<List<Any>> = Api.getData.adminChangePassword(
                        id, dialog.old.text.toString().trim(), dialog.neww.text.toString().trim())

                    call.enqueue(object : Callback<List<Any>> {

                        override fun onResponse(call: Call<List<Any>>?, response: retrofit2.Response<List<Any>>?) {
                            AppLogger.log("checking", response.toString())
                            AppLogger.log("checking2", response!!.body().toString())

                            val array= JSONArray(response.body())
                            val json= array.getJSONObject(0)

                            if (json.getInt("data")== 1) {
                                mSharedPreference.setStringPreferences("password", dialog.neww.text.toString().trim())
                                dialog.cancel()
                                listener.toast("Password Updated Successfully")

                            } else {
                                listener.toast("Password Not Updated!!")

                            }
                        }

                        override fun onFailure(call: Call<List<Any>>?, t: Throwable?) {
                            AppLogger.log("fails", "Failed to add TeamLeader: $t")
                            listener.toast("Failed to update password!!")
                        }
                    })
                }
            }
        }
    }
}
