package com.khairo.teamorganization.ui.teamLeader.main

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import com.khairo.teamorganization.R
import com.khairo.teamorganization.maneger.SharedPreferencesManager
import com.khairo.teamorganization.ui.MainActivity
import com.khairo.teamorganization.ui.teamLeader.accounts.AccountsActivity
import com.khairo.teamorganization.utils.AppLogger
import com.khairo.teamorganization.utils.SlideAnimation
import kotlinx.android.synthetic.main.activity_team_leader.*

class TeamLeaderActivity : AppCompatActivity(),
    TeamLeaderContract {

    private val mSharedPreference: SharedPreferencesManager by lazy { SharedPreferencesManager() }

    private val presenter=
        TeamLeaderPresenter(
            this,
            TeamLeaderInteractor()
        )

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_leader)

        name.text= getString(R.string.welcome)+ " "+ mSharedPreference.getStringPreferences("username", "")

        menu_close.setOnClickListener {
            if (supports.visibility== View.GONE) {
                menuClose(view, 160, 720, 0)

            } else {
                menuClose(view, 720, 160, 1)

            }
        }

        logout.setOnClickListener {
            mSharedPreference.clearPreference()
            MainActivity.start(this)
            finish()
        }

        view_accounts.setOnClickListener {
            AccountsActivity.start(this)
        }
    }

    private fun menuClose(view: View, from: Int, to: Int, type: Int) {
        if (type== 0) {
            menu_close.setImageResource(R.drawable.ic_close)
            supports.visibility= View.VISIBLE

        } else {
            menu_close.setImageResource(R.drawable.ic_menu)
            supports.visibility= View.GONE

        }

        val animation: Animation = SlideAnimation(view, from, to)
        animation.interpolator = AccelerateInterpolator()
        animation.duration = 300
        view.animation = animation
        view.startAnimation(animation)

    }

    private fun getNoAccounts(id: Int) {
        presenter.getNoAccounts(id)
    }

    override fun showProgress() {
        progressBar.visibility= View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility= View.GONE
    }

    override fun toast(message: String) {
        AppLogger.toast(this, message)
    }

    override fun returnNoAccountsAndJobs(account: Int, total: Int, done: Int) {
        number_of_accounts.text= account.toString()
        done_jobs.text= done.toString()
        total_jobs.text= total.toString()
    }

    override fun onResume() {
        super.onResume()

        getNoAccounts(mSharedPreference.getIntPreferences("id", 0))

    }

    companion object {
        fun start(context: Context) {
            val intent= Intent(context, TeamLeaderActivity::class.java)
            context.startActivity(intent)
        }
    }
}
