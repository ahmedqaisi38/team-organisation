package com.khairo.teamorganization.ui.dispatcher

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.khairo.teamorganization.R
import com.khairo.teamorganization.utils.SlideAnimation
import kotlinx.android.synthetic.main.activity_dispatcher.*
import kotlinx.android.synthetic.main.date_picker.*
import java.util.*
import kotlin.collections.ArrayList

class DispatcherActivity : AppCompatActivity() {

    var type = ""
    var types = ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dispatcher)

        types.addAll(resources.getStringArray(R.array.tube_number))
        spinner.setOnClickListener { DialogOffer() }
        tubeNumberSpinner()
        CoreNumberSpinner()
        menu_close_dispatcher.setOnClickListener {
            if (supports.visibility == View.GONE) {
                menuClose(view = view, from = 160, to = 570, type = 0)

            } else {
                menuClose(view, 570, 160, 1)

            }
        }
    }

    private fun DialogOffer() {

        val dialog = Dialog(this)
        dialog.setContentView(R.layout.date_picker)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.show()
        val DatePiker = dialog.datePicker
        val today = Calendar.getInstance()
        DatePiker.init(
            today.get(Calendar.YEAR), today.get(Calendar.MONTH),
            today.get(Calendar.DAY_OF_MONTH)
        ) { view, year, month, day ->
            val month = month + 1
            val msg = "$day/$month/$year"
            spinner.text = msg
            dialog.cancel()
//            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        }


    }

    private fun menuClose(view: View, from: Int, to: Int, type: Int) {
        if (type == 0) {
            menu_close_dispatcher.setImageResource(R.drawable.ic_close)
            supports.visibility = View.VISIBLE

        } else {
            menu_close_dispatcher.setImageResource(R.drawable.ic_menu)
            supports.visibility = View.GONE

        }

        val animation: Animation = SlideAnimation(view, from, to)
        animation.interpolator = AccelerateInterpolator()
        animation.duration = 300
        view.animation = animation
        view.startAnimation(animation)

    }

    fun CoreNumberSpinner() {
        spinner3!!.onItemSelectedListener = TypeClass()
        val arrayAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            resources.getStringArray(R.array.tube_number)
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner3.adapter = arrayAdapter
    }

    fun tubeNumberSpinner() {
        tubeNumber!!.onItemSelectedListener = TypeClass()
        val arrayAdapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item, resources.getStringArray(R.array.tube_number)
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        tubeNumber.adapter = arrayAdapter
    }

    inner class TypeClass : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {

        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            type = types[position]
            if (position == 0) {
//                b_h_j.visibility= View.VISIBLE
            } else {
//                b_h_j.visibility= View.GONE
            }
        }
    }


}
