package com.khairo.teamorganization.ui.teamLeader.accounts

interface AccountContract {
    fun showProgress()
    fun hideProgress()
    fun toast(message:String)
    fun returnJobs(total: Int, done: Int)
    fun returnUsers(users: ArrayList<UsersData>)
}