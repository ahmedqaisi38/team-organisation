package com.khairo.teamorganization.ui.teamLeader.addUser

import android.os.Handler
import com.khairo.teamorganization.retrofit.Api
import com.khairo.teamorganization.utils.AppLogger
import retrofit2.Call
import retrofit2.Callback

class AddUserInteractor {
    interface OnAddFinishedListener {
        fun toast(message: String)
        fun onUserNameError()
        fun onIDError()
        fun onPasswordError()
        fun onBoxError()
        fun onHomeError()
        fun onJointError()
        fun onSuccess(id: Int, username: String, userID: Int, password: String, userType: String,
                      box: String, home: String, joint: String)
    }

    fun checkAddUser(id: Int, username: String, userID: String, password: String, userType: String,
                    box: String, home: String, joint: String, listener: OnAddFinishedListener) {
        Handler().postDelayed({
            when {
                username.isEmpty() -> listener.onUserNameError()
                userID.isEmpty() -> listener.onIDError()
                password.isEmpty() -> listener.onPasswordError()
                userType== "Worker" && box.isEmpty() -> listener.onBoxError()
                userType== "Worker" && home.isEmpty() -> listener.onHomeError()
                userType== "Worker" && joint.isEmpty() -> listener.onJointError()
                else -> listener.onSuccess(id, username, userID.toInt(), password, userType, box, home, joint)
            }
        }, 1000)
    }

    fun addUser(id: Int, username: String, userID: Int, password: String, userType: String, box: String,
                home: String, joint: String, listener: OnAddFinishedListener) {
        val call: Call<List<Any>> = Api.getData.addUser(id, username, userID, password, userType, box, home, joint)
        call.enqueue(object : Callback<List<Any>> {

            override fun onResponse(call: Call<List<Any>>?, response: retrofit2.Response<List<Any>>?) {
                AppLogger.log("checking", response.toString())
                AppLogger.log("checking2", response!!.body().toString())

            }

            override fun onFailure(call: Call<List<Any>>?, t: Throwable?) {
                AppLogger.log("fails", "Failed to add user: $t")
                listener.toast("Failed to add user!!")
            }
        })
    }
}
