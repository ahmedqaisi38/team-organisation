package com.khairo.teamorganization.ui

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.khairo.teamorganization.R
import com.khairo.teamorganization.maneger.SharedPreferencesManager
import com.khairo.teamorganization.ui.admin.AdminActivity
import com.khairo.teamorganization.ui.auth.LoginActivity
import com.khairo.teamorganization.ui.teamLeader.main.TeamLeaderActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val mSharedPreference: SharedPreferencesManager by lazy { SharedPreferencesManager() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (mSharedPreference.getStringPreferences("type", "")== "admin") {
            AdminActivity.start(this)
            finish()

        } else if (mSharedPreference.getStringPreferences("type", "")== "teamLeader") {
            TeamLeaderActivity.start(this)
            finish()
        }

        setContentView(R.layout.activity_main)

        admin_login.setOnClickListener {
            LoginActivity.type= "admin"
            LoginActivity.start(this)
            finish()
        }

        team_leader.setOnClickListener {
            LoginActivity.type= "teamLeader"
            LoginActivity.start(this)
            finish()
        }
    }

    companion object {
        fun start(context: Context) {
            val intent= Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        }
    }
}
