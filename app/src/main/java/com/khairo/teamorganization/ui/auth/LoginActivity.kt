package com.khairo.teamorganization.ui.auth

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.khairo.teamorganization.R
import com.khairo.teamorganization.ui.admin.AdminActivity
import com.khairo.teamorganization.ui.teamLeader.main.TeamLeaderActivity
import com.khairo.teamorganization.utils.AppLogger
import kotlinx.android.synthetic.main.activity_admin_login.*

class LoginActivity : AppCompatActivity(),
    LoginContract {

    private val presenter=
        LoginPresenter(
            this,
            LoginInteractor()
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_login)

        login.setOnClickListener {
            checkSignIn(
                user_id.text.toString().trim(),
                password.text.toString().trim(),
                type
            )
        }
    }

    private fun checkSignIn(id: String, password: String, type: String) {
        presenter.checkSignIn(id, password, type)
    }

    override fun showProgress() {
        progressBar.visibility= View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility= View.GONE
    }

    override fun setIDError() {
        user_id.error= getString(R.string.id_error)
    }

    override fun setPasswordError() {
        password.error= getString(R.string.password_error)
    }

    override fun toast(message: String) {
        AppLogger.toast(this, message)
    }

    override fun goToAdmin() {
        AdminActivity.start(this)
        finish()
    }

    override fun goToLeader() {
        TeamLeaderActivity.start(this)
        finish()
    }

    companion object {
        fun start(context: Context) {
            val intent= Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        }

        var type= ""
    }
}
