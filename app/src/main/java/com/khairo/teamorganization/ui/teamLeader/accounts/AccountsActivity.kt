package com.khairo.teamorganization.ui.teamLeader.accounts

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.khairo.teamorganization.R
import com.khairo.teamorganization.maneger.SharedPreferencesManager
import com.khairo.teamorganization.ui.teamLeader.addUser.AddUserActivity
import com.khairo.teamorganization.utils.AppLogger
import com.khairo.teamorganization.utils.SlideAnimation
import kotlinx.android.synthetic.main.activity_accounts.*

class AccountsActivity : AppCompatActivity(), AccountContract {

    private val mSharedPreference: SharedPreferencesManager by lazy { SharedPreferencesManager() }

    private val presenter= AccountPresenter(this, AccountInteractor())

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accounts)

        name.text= getString(R.string.welcome)+ " "+ mSharedPreference.getStringPreferences("username", "")

        menu_close.setOnClickListener {
            if (supports.visibility== View.GONE) {
                menuClose(view, 160, 570, 0)

            } else {
                menuClose(view, 570, 160, 1)

            }
        }

        add_user.setOnClickListener {
            AddUserActivity.start(this)
        }
    }

    private fun getTotalJobs(id: Int) {
        presenter.getTotalJobs(id)
    }

    private fun getUsers(id: Int) {
        presenter.getUsers(id)
    }

    private fun menuClose(view: View, from: Int, to: Int, type: Int) {
        if (type== 0) {
            menu_close.setImageResource(R.drawable.ic_close)
            supports.visibility= View.VISIBLE

        } else {
            menu_close.setImageResource(R.drawable.ic_menu)
            supports.visibility= View.GONE

        }

        val animation: Animation = SlideAnimation(view, from, to)
        animation.interpolator = AccelerateInterpolator()
        animation.duration = 300
        view.animation = animation
        view.startAnimation(animation)

    }

    override fun showProgress() {
        progressBar.visibility= View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility= View.GONE
    }

    override fun toast(message: String) {
        AppLogger.toast(this, message)
    }

    @SuppressLint("SetTextI18n")
    override fun returnJobs(total: Int, done: Int) {
        jobs2.text= "$done/$total"
        jobs.text= "( $done/$total )"

        val number: Double= ((done.toDouble()/total.toDouble())* 100)
        stats_progressbar.progress= number.toInt()
    }

    override fun returnUsers(users: ArrayList<UsersData>) {
        val adapter = UsersAdapter(this, users)

        rv.layoutManager= LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv.adapter = adapter

        rv.adapter?.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()

        getTotalJobs(
            mSharedPreference.getIntPreferences("id", 0)
        )

        getUsers(
            mSharedPreference.getIntPreferences("id", 0)
        )
    }

    companion object {
        fun start(context: Context) {
            val intent= Intent(context, AccountsActivity::class.java)
            context.startActivity(intent)
        }
    }
}
