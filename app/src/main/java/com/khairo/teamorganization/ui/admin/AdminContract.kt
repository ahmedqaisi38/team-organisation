package com.khairo.teamorganization.ui.admin

interface AdminContract {
    fun showProgress()
    fun hideProgress()
    fun setNameError()
    fun setIDError()
    fun setPasswordError()
    fun toast(message:String)
    fun clear()
}
