package com.khairo.teamorganization.ui.auth

class LoginPresenter(private val loginContract: LoginContract,
                     private val loginInteractor: LoginInteractor
) :
        LoginInteractor.OnSignInFinishedListener {

    fun checkSignIn(id: String, password: String, type: String) {
        loginContract.showProgress()
        loginInteractor.checkSignIn(id, password, type, this)
    }

    override fun toast(message: String) {
        loginContract.apply {
            hideProgress()
            toast(message)
        }
    }

    override fun onIDError() {
        loginContract.apply {
            hideProgress()
            setIDError()
        }
    }

    override fun onPasswordError() {
        loginContract.apply {
            hideProgress()
            setPasswordError()
        }
    }

    override fun onSuccess(id: Int, password: String, type: String) {
        loginInteractor.signIn(id, password, type, this)
    }

    override fun onGoToAdmin() {
        loginContract.apply {
            hideProgress()
            goToAdmin()
        }
    }

    override fun onGoToLeader() {
        loginContract.apply {
            hideProgress()
            goToLeader()
        }
    }
}
