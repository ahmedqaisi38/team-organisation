package com.khairo.teamorganization.ui.teamLeader.main

class TeamLeaderPresenter(private val teamLeaderContract: TeamLeaderContract,
                          private val teamLeaderInteractor: TeamLeaderInteractor) :
        TeamLeaderInteractor.OnTLFinishedListener {

    fun getNoAccounts(id: Int) {
        teamLeaderContract.showProgress()
        teamLeaderInteractor.getNoAccounts(id, this)
    }

    override fun toast(message: String) {
        teamLeaderContract.apply {
            hideProgress()
            toast(message)
        }
    }

    override fun onReturnNoAccountsAndJobs(accounts: Int, total: Int, done: Int) {
        teamLeaderContract.apply {
            hideProgress()
            returnNoAccountsAndJobs(accounts, total, done)
        }
    }
}
