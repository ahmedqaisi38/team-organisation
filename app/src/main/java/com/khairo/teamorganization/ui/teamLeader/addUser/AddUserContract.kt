package com.khairo.teamorganization.ui.teamLeader.addUser

interface AddUserContract {
    fun showProgress()
    fun hideProgress()
    fun setUserNameError()
    fun setIDError()
    fun setPasswordError()
    fun setBoxError()
    fun setHomeError()
    fun setJointError()
    fun toast(message:String)
}