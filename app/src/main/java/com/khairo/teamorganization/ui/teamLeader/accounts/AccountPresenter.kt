package com.khairo.teamorganization.ui.teamLeader.accounts

class AccountPresenter(private val accountContract: AccountContract,
                       private val accountInteractor: AccountInteractor) :
        AccountInteractor.OnAccountFinishedListener {

    fun getTotalJobs(id: Int) {
        accountContract.showProgress()
        accountInteractor.getTotalJobs(id, this)
    }

    fun getUsers(id: Int) {
        accountContract.showProgress()
        accountInteractor.getUsers(id, this)
    }

    override fun toast(message: String) {
        accountContract.apply {
            hideProgress()
            toast(message)
        }
    }

    override fun onReturnJobs(total: Int, done: Int) {
        accountContract.apply {
            hideProgress()
            returnJobs(total, done)
        }
    }

    override fun onReturnUsers(users: ArrayList<UsersData>) {
        accountContract.apply {
            hideProgress()
            returnUsers(users)
        }
    }
}