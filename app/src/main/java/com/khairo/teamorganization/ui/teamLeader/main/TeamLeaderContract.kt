package com.khairo.teamorganization.ui.teamLeader.main

interface TeamLeaderContract {
    fun showProgress()
    fun hideProgress()
    fun toast(message:String)
    fun returnNoAccountsAndJobs(account: Int, total: Int, done: Int)
}