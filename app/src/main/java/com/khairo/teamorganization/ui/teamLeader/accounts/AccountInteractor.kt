package com.khairo.teamorganization.ui.teamLeader.accounts

import com.khairo.teamorganization.retrofit.Api
import com.khairo.teamorganization.utils.AppLogger
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback

class AccountInteractor {
    interface OnAccountFinishedListener {
        fun toast(message: String)
        fun onReturnJobs(total: Int, done: Int)
        fun onReturnUsers(users: ArrayList<UsersData>)
    }

    var users= ArrayList<UsersData>()

    fun getTotalJobs(id: Int, listener: OnAccountFinishedListener) {
        val call: Call<List<Any>> = Api.getData.getTotalJobs(id)
        call.enqueue(object : Callback<List<Any>> {

            override fun onResponse(call: Call<List<Any>>?, response: retrofit2.Response<List<Any>>?) {
                AppLogger.log("checking", response.toString())
                AppLogger.log("checking2", response!!.body().toString())

                val array= JSONArray(response.body())
                val json= array.getJSONObject(0)

                getDoneJobs(id, json.getInt("COUNT(*)"), listener)
            }

            override fun onFailure(call: Call<List<Any>>?, t: Throwable?) {
                AppLogger.log("fails", "Failed to load total jobs: $t")
                listener.toast("Failed to load total jobs!!")
            }
        })
    }

    fun getDoneJobs(id: Int, total: Int, listener: OnAccountFinishedListener) {
        val call: Call<List<Any>> = Api.getData.getDoneJobs(id)
        call.enqueue(object : Callback<List<Any>> {

            override fun onResponse(call: Call<List<Any>>?, response: retrofit2.Response<List<Any>>?) {
                AppLogger.log("checking", response.toString())
                AppLogger.log("checking2", response!!.body().toString())

                val array= JSONArray(response.body())
                val json= array.getJSONObject(0)

                listener.onReturnJobs(total, json.getInt("COUNT(*)"))
            }

            override fun onFailure(call: Call<List<Any>>?, t: Throwable?) {
                AppLogger.log("fails", "Failed to load done jobs: $t")
                listener.toast("Failed to load done jobs!!")
            }
        })
    }

    fun getUsers(id: Int, listener: OnAccountFinishedListener) {
        val call: Call<List<UsersData>> = Api.getData.getUsers(id)
        call.enqueue(object : Callback<List<UsersData>> {

            override fun onResponse(call: Call<List<UsersData>>?, response: retrofit2.Response<List<UsersData>>?) {
                AppLogger.log("checking", response.toString())
                AppLogger.log("checking2", response!!.body().toString())

                users.clear()
                users.addAll(response.body()!!)

                listener.onReturnUsers(users)
            }

            override fun onFailure(call: Call<List<UsersData>>?, t: Throwable?) {
                AppLogger.log("fails", "Failed to load done jobs: $t")
                listener.toast("Failed to load done jobs!!")
            }
        })
    }
}
