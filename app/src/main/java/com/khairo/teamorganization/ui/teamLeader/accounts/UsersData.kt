package com.khairo.teamorganization.ui.teamLeader.accounts

import com.google.gson.annotations.SerializedName

data class UsersData(
    @SerializedName("user_id")
    var user_id: Int,
    @SerializedName("Username")
    var Username: String,
    @SerializedName("Usertype")
    var Usertype: String
)
