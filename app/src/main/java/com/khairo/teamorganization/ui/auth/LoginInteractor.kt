package com.khairo.teamorganization.ui.auth

import android.os.Handler
import com.khairo.teamorganization.maneger.SharedPreferencesManager
import com.khairo.teamorganization.retrofit.Api
import com.khairo.teamorganization.utils.AppLogger
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback

class LoginInteractor {
    interface OnSignInFinishedListener {
        fun toast(message: String)
        fun onIDError()
        fun onPasswordError()
        fun onSuccess(id: Int, password: String, type: String)
        fun onGoToAdmin()
        fun onGoToLeader()
    }

    fun checkSignIn(id: String, password: String, type: String, listener: OnSignInFinishedListener) {
        Handler().postDelayed({
            when {
                id.isEmpty() -> listener.onIDError()
                password.isEmpty() -> listener.onPasswordError()
                else -> listener.onSuccess(id.toInt(), password, type)
            }
        }, 1000)
    }

    fun signIn(id: Int, password: String, type: String, listener: OnSignInFinishedListener) {
        val mSharedPreference: SharedPreferencesManager by lazy { SharedPreferencesManager() }

        val call: Call<List<Any>> = Api.getData.login(id, password, type)
        call.enqueue(object : Callback<List<Any>> {

            override fun onResponse(call: Call<List<Any>>?, response: retrofit2.Response<List<Any>>?) {

                AppLogger.log("checking", response.toString())
                AppLogger.log("checking2", response!!.body().toString())

                val array= JSONArray(response.body())
                val json= array.getJSONObject(0)

                when (type) {
                    "admin" -> {
                        mSharedPreference.setStringPreferences("type", type)
                        mSharedPreference.setIntPreferences("id", json.getInt("user_ID"))
                        mSharedPreference.setStringPreferences("username", json.getString("username"))

                        listener.onGoToAdmin()

                    }

                    "teamLeader" -> {
                        mSharedPreference.setStringPreferences("type", type)
                        mSharedPreference.setIntPreferences("id", json.getInt("TeamLeaderID"))
                        mSharedPreference.setStringPreferences("username", json.getString("Username"))
                        mSharedPreference.setStringPreferences("teamType", json.getString("Team_type"))

                        listener.onGoToLeader()
                    }

                    else -> {

                    }
                }
            }

            override fun onFailure(call: Call<List<Any>>?, t: Throwable?) {
                AppLogger.log("fails", "Failed to login: $t")
                listener.toast("Failed to login")
            }
        })
    }
}
