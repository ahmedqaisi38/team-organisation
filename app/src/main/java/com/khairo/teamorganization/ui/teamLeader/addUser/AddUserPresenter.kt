package com.khairo.teamorganization.ui.teamLeader.addUser

class AddUserPresenter(private val addUserContract: AddUserContract,
                       private val addUserInteractor: AddUserInteractor) :
        AddUserInteractor.OnAddFinishedListener {

    fun checkAddUser(id: Int, username: String, userID: String, password: String, userType: String,
                     box: String, home: String, joint: String) {
        addUserContract.showProgress()
        addUserInteractor.checkAddUser(id, username, userID, password, userType, box, home, joint, this)
    }

    override fun toast(message: String) {
        addUserContract.apply {
            hideProgress()
            toast(message)
        }
    }

    override fun onUserNameError() {
        addUserContract.apply {
            hideProgress()
            setUserNameError()
        }
    }

    override fun onIDError() {
        addUserContract.apply {
            hideProgress()
            setIDError()
        }
    }

    override fun onPasswordError() {
        addUserContract.apply {
            hideProgress()
            setPasswordError()
        }
    }

    override fun onBoxError() {
        addUserContract.apply {
            hideProgress()
            setBoxError()
        }
    }

    override fun onHomeError() {
        addUserContract.apply {
            hideProgress()
            setHomeError()
        }
    }

    override fun onJointError() {
        addUserContract.apply {
            hideProgress()
            setJointError()
        }
    }

    override fun onSuccess(id: Int, username: String, userID: Int, password: String,
                           userType: String, box: String, home: String, joint: String) {
        addUserInteractor.addUser(id, username, userID, password, userType, box, home, joint, this)
    }
}