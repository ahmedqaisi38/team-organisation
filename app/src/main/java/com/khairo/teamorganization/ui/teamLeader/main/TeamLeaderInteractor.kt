package com.khairo.teamorganization.ui.teamLeader.main

import com.khairo.teamorganization.retrofit.Api
import com.khairo.teamorganization.utils.AppLogger
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback

class TeamLeaderInteractor {
    interface OnTLFinishedListener {
        fun toast(message: String)
        fun onReturnNoAccountsAndJobs(accounts: Int, total: Int, done: Int)
    }

    fun getNoAccounts(id: Int, listener: OnTLFinishedListener) {
        val call: Call<List<Any>> = Api.getData.getNoAccounts(id)
        call.enqueue(object : Callback<List<Any>> {

            override fun onResponse(call: Call<List<Any>>?, response: retrofit2.Response<List<Any>>?) {
                AppLogger.log("checking", response.toString())
                AppLogger.log("checking2", response!!.body().toString())

                val array= JSONArray(response.body())
                val json= array.getJSONObject(0)

                getTotalJobs(id, json.getInt("COUNT(*)"), listener)
            }

            override fun onFailure(call: Call<List<Any>>?, t: Throwable?) {
                AppLogger.log("fails", "Failed to load account number: $t")
                listener.toast("Failed to load account number!!")
            }
        })
    }

    fun getTotalJobs(id: Int, accounts: Int, listener: OnTLFinishedListener) {
        val call: Call<List<Any>> = Api.getData.getTotalJobs(id)
        call.enqueue(object : Callback<List<Any>> {

            override fun onResponse(call: Call<List<Any>>?, response: retrofit2.Response<List<Any>>?) {
                AppLogger.log("checking", response.toString())
                AppLogger.log("checking2", response!!.body().toString())

                val array= JSONArray(response.body())
                val json= array.getJSONObject(0)

                getDoneJobs(id, accounts, json.getInt("COUNT(*)"), listener)
            }

            override fun onFailure(call: Call<List<Any>>?, t: Throwable?) {
                AppLogger.log("fails", "Failed to load total jobs: $t")
                listener.toast("Failed to load total jobs!!")
            }
        })
    }

    fun getDoneJobs(id: Int, accounts: Int, total: Int, listener: OnTLFinishedListener) {
        val call: Call<List<Any>> = Api.getData.getDoneJobs(id)
        call.enqueue(object : Callback<List<Any>> {

            override fun onResponse(call: Call<List<Any>>?, response: retrofit2.Response<List<Any>>?) {
                AppLogger.log("checking", response.toString())
                AppLogger.log("checking2", response!!.body().toString())

                val array= JSONArray(response.body())
                val json= array.getJSONObject(0)

                listener.onReturnNoAccountsAndJobs(accounts, total, json.getInt("COUNT(*)"))
            }

            override fun onFailure(call: Call<List<Any>>?, t: Throwable?) {
                AppLogger.log("fails", "Failed to load done jobs: $t")
                listener.toast("Failed to load done jobs!!")
            }
        })
    }
}
