package com.khairo.teamorganization.retrofit

import com.khairo.teamorganization.ui.teamLeader.accounts.UsersData
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @FormUrlEncoded
    @POST("login.php")
    fun login(
        @Field("id") id:Int,
        @Field("password") password:String,
        @Field("type") type:String
    ): Call<List<Any>>

    @FormUrlEncoded
    @POST("addTeamLeader.php")
    fun addTeamLeader(
        @Field("Username") teamName:String,
        @Field("TeamLeaderID") teamID:Int,
        @Field("Password") password:String
    ): Call<List<Any>>

    @FormUrlEncoded
    @POST("adminChangePassword.php")
    fun adminChangePassword(
        @Field("id") id: Int,
        @Field("old") old:String,
        @Field("new") new:String
    ): Call<List<Any>>

    @FormUrlEncoded
    @POST("getNoAccounts.php")
    fun getNoAccounts(
        @Field("id") id:Int
    ): Call<List<Any>>

    @FormUrlEncoded
    @POST("getTotalJobs.php")
    fun getTotalJobs(
        @Field("id") id:Int
    ): Call<List<Any>>

    @FormUrlEncoded
    @POST("getDoneJobs.php")
    fun getDoneJobs(
        @Field("id") id:Int
    ): Call<List<Any>>

    @FormUrlEncoded
    @POST("getUsers.php")
    fun getUsers(
        @Field("id") id:Int
    ): Call<List<UsersData>>

    @FormUrlEncoded
    @POST("addUser.php")// not done
    fun addUser(
        @Field("id") id:Int,
        @Field("username") username:String,
        @Field("userID") userID:Int,
        @Field("password") password:String,
        @Field("userType") userType:String,
        @Field("box") box:String,
        @Field("home") home:String,
        @Field("joint") joint:String
    ): Call<List<Any>>
}
